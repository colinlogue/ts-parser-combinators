### Performance
Calling `Object.freeze` in the `Parser` constructor slowed
down the (admittedly insufficient) benchmark bt ~10% for
`parser3`.