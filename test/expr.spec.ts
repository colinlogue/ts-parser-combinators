import { expect, Test, TestSuite } from "testyts/build/testyCore";
import { equalExpr, Expr, expr } from "../src/example/expr_parse";
import { Parser } from "../src/parser7";


function exprTest<T>(input: string, expected: Expr) {
  const result = expr.run(input);
  expect.toBeTrue(result.isOK);
  expect.toBeTrue(equalExpr(result.unwrap(), expected));
}


@TestSuite('Expression tests')
export class ExprTests {

  @Test('x')
  exprTest1() {
    exprTest(
      'x',
      'x'
    );
  }

  @Test('x + y * z')
  exprTest2() {
    exprTest(
      'x + y * z',
      {
        left: 'x',
        op: '+',
        right: {
          left: 'y',
          op: '*',
          right: 'z'
        }
      }
    )
  }

  @Test('a * b + c ^ d ^ e * f')
  exprTest3() {
    exprTest(
      'a * b + c ^ d ^ e * f',
      {
        left: {
          left: 'a',
          op: '*',
          right: 'b'
        },
        op: '+',
        right: {
          left: {
            left: 'c',
            op: '^',
            right: {
              left: 'd',
              op: '^',
              right: 'e'
            }
          },
          op: '*',
          right: 'f'
        }
      }
    )
  }

}