// import { brackets, char, delimited, int, lower, oneOf, parens, Parser, separated, separatedPair, upper , commaSep } from "../src/parser2";
// import { TestSuite, expect, Test, test } from 'testyts';


// function parserTest<T>(input: string, parser: Parser<[T]>, test: (val: T) => void) {
//   const result = parser.run(input);
//   expect.toBeTrue(result.isOK);
//   test(result.unwrap());
// }


// @TestSuite('Parser tests')
// export class ParserTests {

//   @Test('x')
//   lowerTest1() {
//     parserTest('x', lower, val => expect.toBeEqual(val, 'x'));
//   }

//   @Test('(1,2)')
//   tupleTest1() {
//     parserTest(
//       '(1,2)',
//       parens(separatedPair(int, char(','), int)),
//       val => expect.arraysToBeEqual(val, [1,2])
//     );
//   }

//   @Test('(a,b)')
//   tupleTest2() {
//     parserTest(
//       '(1,x)',
//       parens(separatedPair(int, char(','), lower)),
//       val => expect.arraysToBeEqual(val, [1,'x'])
//     );
//   }

//   @Test('[1,2,3,4]')
//   arrayTest1() {
//     parserTest(
//       '[1,2,3,4]',
//       brackets(commaSep(int)),
//       val => expect.arraysToBeEqual(val, [1,2,3,4])
//     )
//   }

//   @Test('oneOf test 1')
//   oneOfTest1() {
//     const parser = oneOf(char('a'), char('b'), char('c'));
//     parserTest(
//       'a',
//       parser,
//       val => expect.toBeEqual(val, 'a')
//     );
//     parserTest(
//       'b',
//       parser,
//       val => expect.toBeEqual(val, 'b')
//     );
//     parserTest(
//       'c',
//       parser,
//       val => expect.toBeEqual(val, 'c')
//     );
//   }

//   @Test('oneOf test 2')
//   oneOfTest2() {
//     parserTest(
//       'x',
//       oneOf(upper, lower, char('x').map(([val]) => ['y'])),
//       val => expect.toBeEqual(val, 'x')
//     );
//     parserTest(
//       'x',
//       oneOf(upper, char('x').map(([val]) => ['y']), lower),
//       val => expect.toBeEqual(val, 'y')
//     );
//   }

// }