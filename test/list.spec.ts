// import { expect, Test, TestSuite } from "testyts/build/testyCore";
// import { List } from "../src/list";



// @TestSuite('List tests')
// export class StackTests {

//   @Test('empty list is empty')
//   emptyListIsEmpty() {
//     const list: List<[]> = List.empty();
//     expect.toBeTrue(list.isEmpty);
//   }

//   @Test('can add to empty list')
//   canPushToEmptyList() {
//     const list: List<[number]> = List.empty().add(1);
//     expect.toBeTrue(list.head === 1);
//     expect.toBeEqual(list.length, 1);
//   }

//   @Test('can use head value')
//   canUseHeadValue() {
//     const n: number = List.empty().add(1).head;
//     expect.toBeEqual(n, 1);
//   }

//   @Test('can add heterogeneous values')
//   canAddHeterogeneousValues() {
//     const list: List<[number, boolean, string]> =
//       List
//         .empty()
//         .add(1)
//         .add(true)
//         .add('x');
//     expect.toBeEqual(list.head, 'x');
//     expect.toBeEqual(list.drop().head, true);
//     expect.toBeEqual(list.drop().drop().head, 1);
//     expect.toBeEqual(list.drop().drop().drop().head, undefined);
//   }

//   @Test('can map over the head element')
//   canMapOverTheHeadElement() {
//     let list: List<[number, boolean, string]> =
//       List
//         .empty()
//         .add(1)
//         .add(true)
//         .add('x');
//     expect.toBeEqual(list.mapHead(s => s.toUpperCase()).head, 'X');
//     expect.toBeEqual(list.drop().mapHead(b => !b).head, false);
//     expect.toBeEqual(list.drop().drop().mapHead(n => n + 1).head, 2);
//     expect.toBeEqual(list.drop().drop().drop().drop().mapHead(_ => 'a').head, 'a');
//   }

// }