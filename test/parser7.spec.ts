import { brackets, char, int, lower, oneOf, parens, Parser, separated, separatedPair, upper , commaSep, decimalInt, many, decimalDigit } from "../src/parser7_opt";
import { TestSuite, expect, Test } from 'testyts';


function parserTest<T>(input: string, parser: Parser<[T], string>, test: (val: T) => void) {
  const result = parser.run(input);
  expect.toBeTrue(result.isOK);
  test(result.unwrap());
}


@TestSuite('Parser tests')
export class ParserTests {

  @Test('x')
  lowerTest1() {
    parserTest('x', lower, val => expect.toBeEqual(val, 'x'));
  }

  @Test('(1,2)')
  tupleTest1() {
    parserTest(
      '(1,2)',
      parens(separatedPair(int, char(','), int)),
      val => expect.arraysToBeEqual(val, [1,2])
    );
  }

  @Test('(a,b)')
  tupleTest2() {
    parserTest(
      '(1,x)',
      parens(separatedPair(int, char(','), lower)),
      val => expect.arraysToBeEqual(val, [1,'x'])
    );
  }

  @Test('[1,2,3,4]')
  arrayTest1() {
    parserTest(
      '[1,2,3,4]',
      brackets(commaSep(int)),
      val => expect.arraysToBeEqual(val, [1,2,3,4])
    )
  }

  @Test('[]')
  arrayTest2() {
    parserTest(
      '[]',
      brackets(commaSep(decimalInt)),
      val => expect.arraysToBeEqual(val, [])
    )
  }

  @Test('oneOf test 1')
  oneOfTest1() {
    const parser = oneOf(char('a'), char('b'), char('c'));
    parserTest(
      'a',
      parser,
      val => expect.toBeEqual(val, 'a')
    );
    parserTest(
      'b',
      parser,
      val => expect.toBeEqual(val, 'b')
    );
    parserTest(
      'c',
      parser,
      val => expect.toBeEqual(val, 'c')
    );
  }

  @Test('oneOf test 2')
  oneOfTest2() {
    parserTest(
      'x',
      oneOf(upper, lower, char('x').map(([val]) => ['y'])),
      val => expect.toBeEqual(val, 'x')
    );
    parserTest(
      'x',
      oneOf(upper, char('x').map(([val]) => ['y']), lower),
      val => expect.toBeEqual(val, 'y')
    );
  }

  @Test('many test 1')
  manyTest1() {
    parserTest(
      '12345',
      many(decimalDigit),
      val => expect.arraysToBeEqual(val, [1,2,3,4,5])
    )
  }

  @Test('many test 2')
  manyTest2() {
    parserTest(
      '1a2b3c',
      many(oneOf(lower as Parser<[number|string], string>, decimalDigit)),
      val => expect.arraysToBeEqual(val, [1, 'a', 2, 'b', 3, 'c'])
    )
  }

  @Test('stream test 1')
  streamTest1() {
    parserTest(
      '012345',
      many(decimalDigit).stream(Parser.while(x => x < 4)),
      val =>  expect.arraysToBeEqual(val, [0,1,2,3])
    )
  }

  @Test('1234')
  intTest1() {
    parserTest(
      '1234',
      decimalInt,
      val => expect.toBeEqual(val, 1234)
    )
  }

}