import { isAlpha, isAlphaNum, isDigit, isLower, isSpace, isUpper } from "./char";
import { Result } from "./result";


function sharedPrefix(s1: string, s2: string): string {
  let acc = "";
  for (let i = 0; i < s1.length; ++i) {
    if (s1[i] === s2[i]) acc += s1[i];
    else break;
  }
  return acc;
}

export type Top<T extends unknown[]> = T extends [infer V, ...infer U] ? V : void;

export class ParseState<M> {

  input: string;
  position: number;
  memory: M;

  constructor(input: string, mem: M) {
    this.input = input;
    this.position = 0;
    this.memory = mem;
  }

  consume() {
    ++this.position;
  }

  consumeMany(n: number) {
    this.position += n;
  }

  peek(): string | void {
    return this.input[this.position];
  }

  peekMany(n: number): string {
    return this.input.slice(this.position, this.position + n);
  }

  read(): string | void {
    const val = this.input[this.position];
    ++this.position;
    return val;
  }

  readMany(n: number): string {
    const val = this.input.slice(this.position, this.position + n);
    this.position += n;
    return val;
  }

  isFinished(): boolean {
    return this.position >= this.input.length;
  }

  store(mem: M) {
    this.memory = mem;
  }

  lookup() {
    return this.memory;
  }

  update(f: (mem: M) => M): M {
    this.memory = f(this.memory);
    return this.memory;
  }

  next(): IteratorResult<string, void> {
    const value = this.read();
    return value === undefined ? {value, done: true} : {value, done: false};
  }

  [Symbol.iterator] = this;

}

export class ParseError {

  expected: string;
  encountered: string;
  position: number;

  constructor(expected: string, encountered: string, position: number) {
    this.expected = expected;
    this.encountered = encountered;
    this.position = position;
  }

  copy(values?: {expected?: string, encountered?: string, position?: number}): ParseError {
    const expected = values?.expected ?? this.expected;
    const encountered = values?.encountered ?? this.encountered;
    const position = values?.position ?? this.position;
    return new ParseError(expected, encountered, position);
  }

}

export interface ParseResult<T extends unknown[]> {
  consumed: boolean,
  result: Result<T, ParseError>,
}

export type ParseFunc<T extends unknown[], M> = (state: ParseState<M>) => ParseResult<T>;

export function consumedOK<T extends unknown[]>(stack: T): ParseResult<T> {
  return {
    consumed: true,
    result: Result.ok(stack),
  };
}

export function emptyOK<T extends unknown[]>(stack: T): ParseResult<T> {
  return {
    consumed: true,
    result: Result.ok(stack),
  };
}

export function consumedError<T extends unknown[], M>(expected: string, encountered: string, state: ParseState<M>): ParseResult<T> {
  return {
    consumed: true,
    result: Result.err(new ParseError(expected, encountered, state.position)),
  };
}

export function emptyError<T extends unknown[], M>(expected: string, encountered: string, state: ParseState<M>): ParseResult<T> {
  return {
    consumed: false,
    result: Result.err(new ParseError(expected, encountered, state.position)),
  };
}

export class Parser<T extends unknown[], M = unknown> {

  parse: ParseFunc<T,M>;

  constructor(parse: ParseFunc<T,M>) {
    this.parse = parse;
  }

  static pure<T extends unknown[]>(stack: T): Parser<T> {
    return new Parser(state => emptyOK(stack));
  }

  static succeed<V>(value: V): Parser<[V]> {
    return new Parser(state => emptyOK([value]));
  }

  static fail(msg: string): Parser<unknown[]> {
    return new Parser(state => emptyError('', msg, state));
  }

  static any: Parser<[string]> = new Parser(
    state => state.isFinished()
      ? emptyError('any character', 'end of input', state)
      : consumedOK([state.read() as string])
  );

  static end: Parser<[]> = new Parser(
    state => {
      let next = state.peek();
      return next
        ? emptyError('end of input', `'next'`, state)
        : emptyOK([])
    }
  );

  static when(pred: (c: string) => boolean, expected?: string): Parser<[string]> {
    return new Parser(state => {
      const val = state.peek();
      return val && pred(val)
        ? (state.consume(), consumedOK([val]))
        : emptyError(expected ?? '', val ? `'${val}'` : 'end of input', state);
    });
  }

  static while(pred: (c: string) => boolean, options?: {msg?: string, min?: number}): Parser<[string]> {
    return new Parser(state => {
      const min = options?.min ?? 0;
      const msg = options?.msg ?? '';
      let acc = "";
      let peekedVal;
      while (peekedVal = state.peek(), peekedVal && pred(peekedVal))
        acc += state.read();
      const consumed = acc.length > 0;
      const result: Result<[string], ParseError> = acc.length >= min
        ? Result.ok([acc])
        : Result.err(new ParseError(msg, 'insufficient characters', state.position));
      return {consumed, result};
    });
  }

  static store<M>(mem: M): Parser<[],M> {
    return new Parser(state => (state.store(mem), emptyOK([])));
  }

  static lookup<M>(): Parser<[M],M> {
    return new Parser(state => emptyOK([state.lookup()]));
  }

  static update<M>(f: (mem: M) => M): Parser<[M],M> {
    return new Parser(state => emptyOK([state.update(f)]));
  }

  // TODO: Many of these methods create an extra temporary object from the
  // result of parse, which is not used outside of the function. Is there any
  // performance benefit to modifying this object in place an returning it?
  //
  // For example, map could be rewritten as:
  //
  //   map<T2 extends unknown[]>(f: (stack: T) => T2): Parser<T2> {
  //     return new Parser(state => {
  //       const res: ParseResult<any> = this.parse(state);
  //       res.result = res.result.map(f);
  //       return res as ParseResult<T2>;
  //     });
  //   }
  //
  // Note that this requires using some type assertions that are safe because
  // res is never referenced outside of the function, so the fact that is
  // initially a ParseResult<T> and then turned into a ParseResult<T2> does not
  // cause any problems.
  //
  // Further note: An initial benchmark suggested this doesn't do much, if
  // anything, to increase performance. And it seems to be significantly worse
  // in certain circumstances!

  map<T2 extends unknown[]>(f: (stack: T) => T2): Parser<T2,M> {
    return new Parser(state => {
      const {consumed, result} = this.parse(state);
      return {consumed, result: result.map(f)};
    });
  }

  mapError(f: (err: ParseError) => ParseError): Parser<T,M> {
    return new Parser(state => {
      const {consumed, result} = this.parse(state);
      return {consumed, result: result.mapError(f)};
    });
  }

  bind<T2 extends unknown[]>(f: (stack: T) => Parser<T2,M>): Parser<T2,M> {
    return new Parser(state => {
      const {consumed, result} = this.parse(state);
      if (result.isOK)
        return f(result.unwrap()).parse(state);
      else
        // This conversion works because we know the result is an error, so it
        // isn't holding a value of type T. The compiler doesn't see that, so
        // we have to assert it, going through unknown because it doesn't think
        // that T and T2 overlap.
        return {consumed, result} as unknown as ParseResult<T2>;
    });
  }

  then<T2 extends unknown[]>(parser: Parser<T2,M>): Parser<[...T2, ...T],M> {
    return new Parser(state => {
      const {consumed, result} = this.parse(state);
      if (result.isOK) {
        const vals1 = result.unwrap();
        const {consumed, result: result2} = parser.parse(state);
        return {consumed, result: result2.map(vals2 => [...vals2, ...vals1])};
      }
      // Same note as bind for this assertion.
      else return {consumed, result} as unknown as ParseResult<[...T2, ...T]>;
    });
  }

  skip(parser: Parser<any,any>): Parser<T,M> {
    return new Parser(state => {
      const {consumed, result} = this.parse(state);
      if (result.isOK)
        void parser.parse(state);
      return {consumed, result};
    });
  }

  expect(expected: string): Parser<T,M> {
    return new Parser(state => {
      const {consumed, result} = this.parse(state);
      return {consumed, result: result.mapError(err => err.copy({expected}))};
    });
  }

  run(input: string, mem: M): Result<Top<T>, ParseError> {
    const state = new ParseState(input, mem);
    return this.parse(state).result.map(stack => stack[0] as Top<T>);
  }

}

/**
 * Takes a list of parsers, and tries them one at a time until either one
 * succeeds or one fails after consuming input.
 */
export function oneOf<T extends unknown[]>(...parsers: Parser<T>[]): Parser<T> {
  return new Parser(state => {
    for (const parser of parsers) {
      const {consumed, result} = parser.parse(state);
      if (result.isOK || consumed)
        return {consumed, result};
    }
    return emptyError('', 'no matching parser', state);
  });
}

export function between<T extends unknown[], M>(start: Parser<any,M>, inside: Parser<T,M>, end: Parser<any,M>): Parser<T,M> {
  return skip(start).then(inside).skip(end);
}

export function skip<M>(parser: Parser<any,M>): Parser<[],M> {
  return parser.map(_ => []);
}

export function char(c: string): Parser<[string], any> {
  return Parser.when(c2 => c === c, `'${c}'`);
}

export const leftParen: Parser<[string]> = char('(');
export const rightParen: Parser<[string]> = char(')');

export function parens<T extends unknown[]>(parser: Parser<T>): Parser<T> {
  return between(leftParen, parser, rightParen);
}

export const leftBrace: Parser<[string]> = char('{');
export const rightBrace: Parser<[string]> = char('}');

export function braces<T extends unknown[]>(parser: Parser<T>): Parser<T> {
  return between(leftBrace, parser, rightBrace);
}

export const leftBracket: Parser<[string]> = char('[');
export const rightBracket: Parser<[string]> = char(']');

export function brackets<T extends unknown[]>(parser: Parser<T>): Parser<T> {
  return between(leftBracket, parser, rightBracket);
}

export const leftAngle: Parser<[string]> = char('<');
export const rightAngle: Parser<[string]> = char('>');

export function angles<T extends unknown[]>(parser: Parser<T>): Parser<T> {
  return between(leftAngle, parser, rightAngle);
}

export const doubleQuote: Parser<[string]> = char('"');

export function doubleQuotes<T extends unknown[]>(parser: Parser<T>): Parser<T> {
  return between(doubleQuote, parser, doubleQuote);
}

export function exact(str: string): Parser<[string]> {
  return new Parser(state =>{
    if (str === '') return emptyOK(['']);
    const first = state.peek();
    if (first !== str[0])
      return emptyError(`"${str}"`, first ? `'${first}'` : 'end of input', state);
    const word = state.readMany(str.length);
    if (word === str)
      return consumedOK([str]);
    else {
      const sharedLength = sharedPrefix(str, word).length;
      // Rewind the position to the point of divergence.
      state.position -= word.length - sharedLength;
      const diverged = word.slice(0, sharedLength + 1);
      return consumedError(`"${str}"`, `"${diverged}"`, state);
    }
  });
}

export function digit(radix: number): Parser<[number]> {
  return Parser.when(isDigit).map(([c]) => [parseInt(c, radix)]);
}

export const decimalDigit: Parser<[number]> = digit(10);
export const hexDigit: Parser<[number]> = digit(16);
export const octalDigit: Parser<[number]> = digit(8);
export const binaryDigit: Parser<[number]> = digit(2);

export function intBase(radix: number): Parser<[number]> {
  return Parser.while(c => isDigit(c, radix), {min: 1}).map(([s]) => [parseInt(s, radix)]);
}

export const decimalInt: Parser<[number]> = intBase(10);
export const hexInt: Parser<[number]> = intBase(16);
export const octalInt: Parser<[number]> = intBase(8);
export const binaryInt: Parser<[number]> = intBase(2);

export const int: Parser<[number]> = decimalInt;

export const alpha: Parser<[string]> = Parser.when(isAlpha);
export const lower: Parser<[string]> = Parser.when(isLower);
export const upper: Parser<[string]> = Parser.when(isUpper);
export const alphaNum: Parser<[string]> = Parser.when(isAlphaNum);

export function separated<T>(item: Parser<[T]>, sep: Parser<any>): Parser<[T[]]> {
  return new Parser(state => {
    const acc: T[] = [];
    let {consumed, result} = item.parse(state);
    const parser: Parser<[T]> = skip(sep).then(item);
    // Continue parsing until it fails.
    while (result.isOK) {
      acc.push(result.unwrap()[0]);
      ({consumed, result} = parser.parse(state));
    }
    // If the failure happened without consuming input, then we are fine and
    // return the accumulated result. Otherwise, return the error.
    if (consumed)
      // See the note on Parser.bind for why this is safe.
      return {consumed, result} as unknown as ParseResult<[T[]]>;
    else
      return {consumed, result: Result.ok([acc])}
  });
}

export function commaSep<T>(item: Parser<[T]>): Parser<[T[]]> {
  return separated(item, char(','));
}

export function semiSep<T>(item: Parser<[T]>): Parser<[T[]]> {
  return separated(item, char(','));
}

export function separatedPair<T1, T2, U extends unknown[]>(left: Parser<[T1]>, sep: Parser<U>, right: Parser<[T2]>): Parser<[[T1,T2]]> {
  return left.skip(sep).then(right).map(([r,l]) => [[l,r]]);
}