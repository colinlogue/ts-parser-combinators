import { Head, List, Rest, SwapHead } from "./list";
import { Result } from "./result";

type ParseState<S extends unknown[]> = {
  rest: string,
  stack: List<S>,
};

type ParseError = string;
type ParseResult<S extends unknown[]> = Result<ParseState<S>,ParseError>;

type ParserFunc<S1 extends unknown[], S2 extends unknown[]> = (state: ParseState<S1>) => ParseResult<S2>;

export class Parser<S1 extends unknown[], S2 extends unknown[]> {

  parse: ParserFunc<S1,S2>;

  constructor (func: ParserFunc<S1,S2>) {
    this.parse = func;
  }

  static empty<S extends unknown[]>(): Parser<S,S> {
    return new Parser(Result.ok);
  }

  static succeed<S extends unknown[],T>(val: T): Parser<S, [...S, T]> {
    return new Parser(
      ({rest, stack}) =>
        Result.ok({rest, stack: stack.add(val)}));
  }

  static fail<S extends unknown[]>(msg: string): Parser<S,S> {
    return new Parser(_ => Result.err(msg));
  }

  static chomp<S extends unknown[]>(n = 1): Parser<S, [...S, string]> {
    return new Parser(
      ({rest, stack}) =>
        rest.length > 0
          ? Result.ok({rest: rest.slice(1), stack: stack.add(rest.slice(0,1))})
          : Result.err("can't chomp empty input"));
  }

  static chompWhile<S extends unknown[]>(pred: (param: string) => boolean): Parser<S, [...S, string]> {
    return new Parser(
      ({rest, stack}) => {
        let i = 0;
        const limit = rest.length;
        while (i < limit && pred(rest[i])) ++i;
        return Result.ok({rest: rest.slice(i), stack: stack.add(rest.slice(0,i))});
      });
  }

  map<T>(f: (top: Head<S2>) => T): Parser<S1, SwapHead<S2,T>> {
    return new Parser(init =>
      this.parse(init).map(
        ({rest, stack}) => ({rest, stack: stack.mapHead(f)}))
    );
  }

  mapStack<S3 extends unknown[]>(f: (stack: S2) => S3): Parser<S1,S3> {
    return new Parser(init =>
      this.parse(init).map(
        ({rest, stack}) => ({rest, stack: new List(f(stack.data))})
      ));
  }

  bind<S3 extends unknown[]>(f: (param: Head<S2>) => Parser<S2,S3>): Parser<S1,S3> {
    return new Parser(init => this.parse(init).bind(({rest, stack}) => f(stack.head).parse({rest, stack})));
  }

  then<S3 extends unknown[]>(parser: Parser<S2,S3>): Parser<S1,S3> {
    return new Parser(init => this.parse(init).bind(({rest, stack}) => parser.parse({rest, stack})));
  }

  or(alt: Parser<S1,S2>): Parser<S1,S2> {
    return new Parser(init => {
      let res = this.parse(init);
      if (res.isOK)
        return res;
      else
        return alt.parse(init);
    });
  }

  assert(pred: (val: Head<S2>) => boolean, msg = "assertion failed"): Parser<S1,S2> {
    return new Parser(init => this.parse(init).bind(
      ({rest, stack}) =>
        pred(stack.head)
          ? Result.ok({rest, stack})
          : Result.err(msg)));
  }

  peek(n = 1): Parser<S1, [...S2, string]> {
    return new Parser(init => this.parse(init).bind(({rest, stack}) => Result.ok({rest: rest.slice(0,n), stack: stack.add(rest.slice(n))})));
  }

  drop(): Parser<S1, Rest<S2>> {
    return new Parser(init => this.parse(init).bind(({rest, stack}) => Result.ok({rest, stack: stack.drop()})));
  }

  expects(descr: string): Parser<S1,S2> {
    return new Parser(init => this.parse(init).mapError(_ => `expected ${descr}`))
  }

  run(input: string): Result<Head<S2>,ParseError> {
    return this.parse({rest: input, stack: List.empty()}).map(({stack}) => stack.head);
  }

}
