// Needed until es2022 target added.
declare interface Array<T> {
  at(index: number): T | undefined;
}

export type SwapHead<S extends unknown[], V> = S extends [...infer T, infer U] ? [...T, V] : [];

export type Head<S extends unknown[]> = S extends [...infer T, infer U] ? U : void;
export type Rest<S extends unknown[]> = S extends [...infer T, infer U] ? T : [];

export class List<S extends unknown[]> {

  data: S;

  constructor(data?: S) {
    this.data = data ?? [] as unknown as S;
  }

  static empty<S extends unknown[]>(): List<S> {
    return new List();
  }

  get isEmpty(): boolean {
    return this.data.length === 0;
  }

  get length(): number {
    return this.data.length;
  }

  get head(): Head<S> {
    const i = this.data.length - 1;
    if (i < 0) return undefined as Head<S>;
    else return this.data[i] as Head<S>;
  }

  get rest(): Rest<S> {
    return this.data.slice(0,-1) as Rest<S>;
  }

  add<U>(val: U): List<[...S,U]> {
    const data = this.data.slice();
    data.push(val);
    return new List(data as [...S, U]);
  }

  drop(): List<Rest<S>> {
    return new List(this.rest);
  }

  mapHead<T>(f: (h: Head<S>) => T): List<SwapHead<S,T>> {
    const data = this.rest as any[];
    if (this.data.length > 0)
      data.push(f(this.head));
    return new List(data as SwapHead<S,T>);
  }

}

export function fromRest<S extends unknown[], T>(rest: Rest<[...S, T]>): S {
  return rest as unknown as S;
}