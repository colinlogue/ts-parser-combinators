

export function isDigit(c: string, radix = 10): boolean {
  const code = c.charCodeAt(0);
  return !(isNaN(parseInt(c, radix)));
}

export function isUpper(c: string): boolean {
  const code = c.charCodeAt(0);
  return code > 64 && code < 91;
}

export function isLower(c: string): boolean {
  const code = c.charCodeAt(0);
  return code > 96 && code < 123;
}

export function isAlpha(c: string): boolean {
  const code = c.charCodeAt(0);
  return (code > 64 && code < 91) || (code > 96 && code < 123);
}

export function isAlphaNum(c: string): boolean {
  const code = c.charCodeAt(0);
  return (code > 47 && code < 58) || (code > 64 && code < 91) || (code > 96 && code < 123);
}

export function isSpace(c: string): boolean {
  const code = c.charCodeAt(0);
  return (code > 8 && code < 14) || code == 32 || code == 133 || code == 160;
}