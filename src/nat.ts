
type Nat = Zero | NonZeroNat;
type NonZeroNat = [Nat];

type Zero = 0;
type Succ<N> = [N];
type Pred<N> = N extends Succ<infer M> ? M : Zero;

type Add<M,N> = M extends Succ<infer L> ? Succ<Add<L,N>> : N;
type Subtract<M,N> = N extends Succ<infer P> ? Pred<Subtract<M,P>> : M;

