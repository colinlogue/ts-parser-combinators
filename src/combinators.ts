import { isAlpha, isAlphaNum, isDigit, isLower, isSpace, isUpper } from "./char";
import { fromRest, List, Rest } from "./list";
import { Parser } from "./parser";
import { Result } from "./result";

type SimpleParser<S extends unknown[], T> = Parser<S, [...S, T]>;

export function many<S extends unknown[], T>(item: Parser<S, [...S, T]>, options?: {min?: number, acc?: T[]}): Parser<S, [...S, T[]]> {
  const acc = options?.acc ?? [];
  const min = options?.min ?? 0;
  return new Parser(init => {
    let _rest = init.rest;
    let _stack = init.stack;
    let res = item.parse(init);
    while (res.isOK) {
      const {rest, stack} = res.unwrap();
      acc.push(stack.head as T);
      _rest = rest;
      res = item.parse({rest, stack: _stack});
    }
    return acc.length >= min
      ? Result.ok({rest: _rest, stack: _stack.add(acc)})
      : Result.err(`got ${acc.length} elements in many with min ${min}`);
  });
}

export function delimited<S1 extends unknown[], S2 extends unknown[], U1 extends unknown[], U2 extends unknown[]>(start: Parser<S1,U1>, inside: Parser<S1,S2>, end: Parser<S2,U2>): Parser<S1,S2> {
  return ignore(start).then(inside).then(ignore(end));
}

export function parens<S1 extends unknown[], S2 extends unknown[]>(inside: Parser<S1,S2>): Parser<S1,S2> {
  return delimited(char('('), inside, char(')'));
}

export function char<S extends unknown[]>(c: string): Parser<S, [...S, string]> {
  return Parser.chomp<S>().assert(c2 => c === c2, `expected ${c}`);
}

export function ignore<S1 extends unknown[], S2 extends unknown[]>(parser: Parser<S1,S2>): Parser<S1,S1> {
  return new Parser(
    init => parser.parse(init).map(({rest}) => ({rest, stack: init.stack}))
  );
}

export function combine<S extends unknown[], T1, T2>(): Parser<[...S, T1, T2], [...S, [T1, T2]]> {
  return Parser.empty().mapStack(stack => {
    const data = stack.slice();
    const x2 = data.pop();
    const x1 = data.pop();
    data.push([x1,x2]);
    return data as [...S, [T1, T2]];
  });
}

export function pair<S extends unknown[], T1, T2>(left: Parser<S, [...S, T1]>, right: Parser<[...S, T1], [...S, T1, T2]>): Parser<S, [...S, [T1, T2]]> {
  return left.then(right).then(combine());
}

export function separatedPair<S extends unknown[], T1, T2, U extends unknown[]>(left: Parser<S, [...S, T1]>, sep: Parser<[...S, T1],U>, right: Parser<[...S, T1], [...S, T1, T2]>): Parser<S, [...S, [T1, T2]]> {
  return left
    .then(ignore(sep))
    .then(right)
    .then(combine());
}

export function separatedList<S extends unknown[], T, U extends unknown[]>(item: Parser<S, [...S, T]>, sep: Parser<S,U>): Parser<S, [...S, T[]]> {
  return new Parser(init => {
    const first = item.parse(init);
    if (first.isOK) {
      const {rest, stack} = first.unwrap();
      const acc: T[] = [stack.head as T];
      const _stack: List<S> = new List(fromRest(stack.rest));
      return many(ignore(sep).then(item), {acc}).parse({rest, stack: _stack});
    }
    else return Result.ok({rest: init.rest, stack: init.stack.add([])});
  });
}

// export const whitespace: Parser<S,S> = ignore(Parser.chompWhile(isSpace));

// export const int: Parser<number> =
//   Parser
//     .chompWhile(isDigit)
//     .map(s => parseInt(s, 10));

// export function digit(radix = 10) {
//   return Parser
//     .chomp()
//     .map(val => parseInt(val, radix))
//     .assert(val => !(isNaN(val)))
//     .expects(`base ${radix} digit`);
// }

// export const binaryDigit: Parser<number> = digit(2);

// export const decimalDigit: Parser<number> = digit(10);

// export const hexDigit: Parser<number> = digit(16);

// export const octalDigit: Parser<number> = digit(8);

// export function intBase(radix: number) {
//   return Parser
//     .chompWhile(c => isDigit(c, radix))
//     .map(s => parseInt(s, radix))
//     .expects(`base ${radix} int`);
// }

// export const binaryInt: Parser<number> = intBase(2);

// export const decimalInt: Parser<number> = intBase(10);

// export const hexInt: Parser<number> = intBase(16);

// export const octalInt: Parser<number> = intBase(8);

// export const lower: Parser<string> = Parser.chomp().assert(isLower);

// export const upper: Parser<string> = Parser.chomp().assert(isUpper);

// export const alpha: Parser<string> = Parser.chomp().assert(isAlpha);

// export const alphaNum: Parser<string> = Parser.chomp().assert(isAlphaNum);

// export function lexeme<T>(parser: Parser<T>): Parser<T> {
//   return delimited(whitespace, parser, whitespace);
// }

// export function oneOf<T>(first: Parser<T>, ...alts: Parser<T>[]): Parser<T> {
//   return alts.reduce((prev, cur) => prev.or(cur), first);
// }