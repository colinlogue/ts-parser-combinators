




function left<L,R>(tuple: [L, R]): L {
  return tuple[0];
}

function right<L,R>(tuple: [L, R]): R {
  return tuple[1];
}