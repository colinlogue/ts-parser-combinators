import { isAlpha, isAlphaNum, isDigit, isLower, isSpace, isUpper } from "./char";
import { Result } from "./result"

export type Top<T extends unknown[]> = T extends [infer U, ...infer V] ? U : void;

export type ParseError = string;

export type ParseResult<T extends unknown[]> = Result<[string, T], ParseError>;

export type ParseFunction<T extends unknown[]> = (input: string) => ParseResult<T>;

export class Parser<T extends unknown[]> {

  parse: ParseFunction<T>;

  constructor(parse: ParseFunction<T>) {
    this.parse = parse;
  }

  static empty: Parser<[]> = new Parser (s => Result.ok([s, []]));

  static succeed<T>(val: T): Parser<[T]> {
    return new Parser(s => Result.ok([s, [val]]));
  }

  static succeeds<T extends unknown[]>(vals: T): Parser<T> {
    return new Parser(s => Result.ok([s, vals]));
  }

  static fail<T extends unknown[]>(msg: string): Parser<T> {
    return new Parser(_ => Result.err(msg));
  }

  static chomp(n = 1): Parser<[string]> {
    return new Parser(s =>
      s.length >= n
        ? Result.ok([s.slice(n), [s.slice(0,n)]])
        : Result.err('attempted to chomp past end of input')
    );
  }

  static chompIf(pred: (c: string) => boolean): Parser<[string]> {
    return Parser.chomp().bind(
      ([c]) =>
        pred(c)
          ? Parser.succeed(c)
          : Parser.fail('chomp failed')
    );
  }

  static chompWhile(pred: (c: string) => boolean): Parser<[string]> {
    return new Parser(s => {
      let i = 0;
      while (i < s.length && pred(s[i])) ++i;
      return Result.ok([s.slice(i), [s.slice(0,i)]]);
    });
  }

  static chompMap<T>(f: (c: string) => T | null): Parser<[T]> {
    return Parser.chomp().bind(
      ([c]) => {
        const val = f(c);
        return val === null
          ? Parser.fail('chomp map failed')
          : Parser.succeed(val);
      }
    );
  }

  or(alt: Parser<T>): Parser<T> {
    return new Parser(s => {
      const res = this.parse(s);
      if (res.isOK)
        return res;
      else
        return alt.parse(s);
    });
  }

  map<T2 extends unknown[]>(f: (vals: T) => T2): Parser<T2> {
    return new Parser(s => this.parse(s).map(([rest, vals]) => [rest, f(vals)]));
  }

  bind<T2 extends unknown[]>(f: (vals: T) => Parser<T2>): Parser<T2> {
    return new Parser(s => this.parse(s).bind(([rest, vals]) => f(vals).parse(rest)));
  }

  then<T2 extends unknown[]>(parser: Parser<T2>): Parser<[...T2, ...T]> {
    return new Parser(s => this.parse(s).bind(
      ([rest1, vals1]) => parser.parse(rest1).map(
      ([rest2, vals2]) => ([rest2, [...vals2, ...vals1]]))
    ));
  }

  ignore<T2 extends unknown[]>(parser: Parser<T2>): Parser<T> {
    return new Parser(s => this.parse(s).bind(
      ([rest1, vals]) => parser.parse(rest1).map(
      ([rest2, _]) => ([rest2, vals]))
    ));
  }

  assert(pred: (vals: T) => boolean): Parser<T> {
    return new Parser(s => this.parse(s).bind(
      ([rest, vals]) =>
        pred(vals)
          ? Result.ok([rest, vals])
          : Result.err('assertion failed')
    ));
  }

  run(input: string): Result<Top<T>, ParseError> {
    return this.parse(input).map(
      ([_, vals]) => vals[0] as Top<T>
    );
  }

}

export const whitespace: Parser<[]> = Parser.empty.ignore(Parser.chompWhile(isSpace));

export function many<T>(item: Parser<[T]>, constraints?: {min?: number, max?: number}): Parser<[T[]]> {
  return new Parser(s => {
    let min = constraints?.min ?? 0;
    let max = constraints?.max ?? Infinity;
    const acc: T[] = [];
    let rest = s;
    let res = item.parse(s);
    while (res.isOK && acc.length < max) {
      let val: T;
      [rest, [val]] = res.unwrap();
      acc.push(val);
      res = item.parse(rest);
    }
    return acc.length >= min
      ? Result.ok([rest, [acc]])
      : Result.err('did not reach min in many');
  });
}

export function delimited<T1 extends unknown[], T2 extends unknown[], T3 extends unknown[]>
  (start: Parser<T1>, inner: Parser<T2>, end: Parser<T3>): Parser<T2>
{
  return Parser.empty
    .ignore(start)
    .then(inner)
    .ignore(end)
}

export function char(c: string): Parser<[string]> {
  return Parser.chompIf(c2 => c === c2);
}

export function exact(str: string): Parser<[string]> {
  return new Parser(s => {
    return s.startsWith(str)
      ? Result.ok([s.slice(str.length), [str]])
      : Result.err(`input does not start with ${str}`);
  });
}

export function digit(radix: number): Parser<[number]> {
  return Parser.chompMap(c => {
    let val = parseInt(c, radix);
    return isNaN(val)
      ? null
      : val;
    });
}

export const decimalDigit: Parser<[number]> = digit(10);
export const hexDigit: Parser<[number]> = digit(16);
export const octalDigit: Parser<[number]> = digit(8);
export const binaryDigit: Parser<[number]> = digit(2);

export function intBase(radix: number): Parser<[number]> {
  return Parser.chompWhile(c => isDigit(c, radix)).map(([s]) => [parseInt(s, radix)]);
}

export const decimalInt: Parser<[number]> = intBase(10);
export const hexInt: Parser<[number]> = intBase(16);
export const octalInt: Parser<[number]> = intBase(8);
export const binaryInt: Parser<[number]> = intBase(2);

export const int: Parser<[number]> = decimalInt;

export const alpha: Parser<[string]> = Parser.chompIf(isAlpha);
export const lower: Parser<[string]> = Parser.chompIf(isLower);
export const upper: Parser<[string]> = Parser.chompIf(isUpper);
export const alphaNum: Parser<[string]> = Parser.chompIf(isAlphaNum);

export function parens<T extends unknown[]>(inside: Parser<T>): Parser<T> {
  return delimited(char('('), inside, char(')'));
}

export function braces<T extends unknown[]>(inside: Parser<T>): Parser<T> {
  return delimited(char('{'), inside, char('}'));
}

export function brackets<T extends unknown[]>(inside: Parser<T>): Parser<T> {
  return delimited(char('['), inside, char(']'));
}

export function angles<T extends unknown[]>(inside: Parser<T>): Parser<T> {
  return delimited(char('<'), inside, char('>'));
}

export function separated<T,U extends unknown[]>(item: Parser<[T]>, sep: Parser<U>): Parser<[T[]]> {
  return new Parser(s => {
    const res = item.parse(s);
    if (res.isOK) {
      const [rest1, [val]] = res.unwrap();
      // Note that this unwrap is safe because many without min always succeeds.
      let [rest2, [vals]] = many(ignored(sep).then(item)).parse(rest1).unwrap();
      vals.unshift(val);
      return Result.ok([rest2, [vals]]);
    }
    else return Result.ok([s, [[]]]);
  });
}

export function commaSep<T>(item: Parser<[T]>): Parser<[T[]]> {
  return separated(item, char(','));
}

export function semiSep<T>(item: Parser<[T]>): Parser<[T[]]> {
  return separated(item, char(','));
}

export function separatedPair<T1, T2, U extends unknown[]>(left: Parser<[T1]>, sep: Parser<U>, right: Parser<[T2]>): Parser<[[T1,T2]]> {
  return left.ignore(sep).then(right).map(([r,l]) => [[l,r]]);
}

export function oneOf<T extends unknown[]>(...alts: Parser<T>[]): Parser<T> {
  return alts.reduce((prev, cur) => prev.or(cur));
}

export function ignored<U extends unknown[]>(parser: Parser<U>): Parser<[]> {
  return parser.map(_ => []);
}