import { isAlpha, isAlphaNum, isDigit, isLower, isSpace, isUpper } from "./char";
import { Result } from "./result";


function sharedPrefix(s1: string, s2: string): string {
  let acc = "";
  for (let i = 0; i < s1.length; ++i) {
    if (s1[i] === s2[i]) acc += s1[i];
    else break;
  }
  return acc;
}

export type Streamable<Tok> = Tok extends string ? (string[] | string) : Tok[];

export type Top<T extends unknown[]> = T extends [infer V, ...infer U] ? V : void;

export type Rest<T extends unknown[]> = T extends [infer V, ...infer U] ? U : [];

export const END_OF_INPUT = Symbol('end of input');

export type END_OF_INPUT = typeof END_OF_INPUT;

export interface Stream<S> {
  [pos: number]: Token<S>|void,
  slice(start?: number, end?: number): S,
  length: number,
}

export type Token<T> = T extends (infer U)[] ? U : T extends string ? string : never;

export type Acc<Tok> = Tok extends string ? string : Tok[];

export abstract class ParseState<S> {

  // Valid positions are in the range [0, input.length].
  // If the position is at input.length, then reading returns END_OF_INPUT.

  input: Stream<S>;
  position: number;

  constructor(input: Stream<S>) {
    this.input = input;
    this.position = 0;
  }

  advance() {
    ++this.position;
  }

  advanceMany(n: number) {
    this.position = Math.min(this.position + n, this.input.length);
  }

  peek(): Token<S> | END_OF_INPUT {
    if (this.isFinished()) return END_OF_INPUT;
    return this.input[this.position] as Token<S>;
  }

  peekMany(n: number): S {
    return this.input.slice(this.position, this.position + n);
  }

  read(): Token<S> | END_OF_INPUT {
    if (this.position >= this.input.length)
      return END_OF_INPUT;
    const val = this.input[this.position] as Token<S>;
    this.advance();
    return val;
  }

  readMany(n: number): S {
    const val = this.input.slice(this.position, this.position + n);
    this.advanceMany(n);
    return val;
  }

  isFinished(): boolean {
    return this.position === this.input.length;
  }

  next(): IteratorResult<Token<S>, END_OF_INPUT> {
    const value = this.read();
    return value === END_OF_INPUT
      ? ({value, done: true} as IteratorReturnResult<END_OF_INPUT>)
      : {value, done: false};
  }

  [Symbol.iterator] = this;

}

export class ParseStateString extends ParseState<string> {

  advance() {
    ++this.position;
  }

  advanceMany(n: number) {
    this.position = Math.min(this.position + n, this.input.length);;
  }

  peek(): string | END_OF_INPUT {
    if (this.isFinished()) return END_OF_INPUT;
    return this.input[this.position] as string;
  }

  peekMany(n: number): string {
    return this.input.slice(this.position, this.position + n);
  }

  read(): string | END_OF_INPUT {
    if (this.position >= this.input.length)
      return END_OF_INPUT;
    const val = this.input[this.position] as string;
    ++this.position;
    return val;
  }

  readMany(n: number): string {
    const val = this.input.slice(this.position, this.position + n);
    this.position = Math.min(this.position + n, this.input.length);
    return val;
  }

  isFinished(): boolean {
    return this.position === this.input.length;
  }

  next(): IteratorResult<string, END_OF_INPUT> {
    const value = this.read();
    return value === END_OF_INPUT
      ? ({value, done: true} as IteratorReturnResult<END_OF_INPUT>)
      : {value, done: false};
  }

  [Symbol.iterator] = this;

}

export class ParseError {

  expected: string;
  encountered: string;
  position: number;

  constructor(expected: string, encountered: string, position: number) {
    this.expected = expected;
    this.encountered = encountered;
    this.position = position;
  }

  copy(values?: {expected?: string, encountered?: string, position?: number}): ParseError {
    const expected = values?.expected ?? this.expected;
    const encountered = values?.encountered ?? this.encountered;
    const position = values?.position ?? this.position;
    return new ParseError(expected, encountered, position);
  }

}

export interface ParseResult<T extends unknown[]> {
  consumed: boolean,
  result: Result<T, ParseError>,
}

export type ParseFunc<T extends unknown[], Tok> = (state: ParseState<Tok>) => ParseResult<T>;

export function consumedOK<T extends unknown[]>(stack: T): ParseResult<T> {
  return {
    consumed: true,
    result: Result.ok(stack),
  };
}

export function emptyOK<T extends unknown[]>(stack: T): ParseResult<T> {
  return {
    consumed: true,
    result: Result.ok(stack),
  };
}

export function consumedError<T extends unknown[], Tok>(expected: string, encountered: string, state: ParseState<Tok>): ParseResult<T> {
  return {
    consumed: true,
    result: Result.err(new ParseError(expected, encountered, state.position)),
  };
}

export function emptyError<T extends unknown[], Tok>(expected: string, encountered: string, state: ParseState<Tok>): ParseResult<T> {
  return {
    consumed: false,
    result: Result.err(new ParseError(expected, encountered, state.position)),
  };
}

export class Parser<T extends unknown[], S> {

  parse: ParseFunc<T,S>;

  constructor(parse: ParseFunc<T,S>) {
    this.parse = parse;
  }

  static pure<T extends unknown[], S>(stack: T): Parser<T,S> {
    return new Parser(state => emptyOK(stack));
  }

  static succeed<V,S>(value: V): Parser<[V],S> {
    return new Parser(state => emptyOK([value]));
  }

  static fail(msg: string): Parser<unknown[],unknown> {
    return new Parser(state => emptyError('', msg, state));
  }

  /**
   * Unfortunately, I don't think there is a way to indicate to Typescript that
   * the two unknown types here are both the Tok type. So it requires casting
   * the result to use this one.
   */
  static any: Parser<[unknown],unknown> = new Parser(
    state =>
      state.isFinished()
        ? emptyError('any token', 'end of input', state)
        : consumedOK([state.read()])
  );

  static end: Parser<[],unknown> = new Parser(
    state => {
      let next = state.peek();
      return next
        ? emptyError('end of input', `'next'`, state)
        : emptyOK([])
    }
  );

  static when<S>(pred: (c: Token<S>) => boolean, expected?: string): Parser<[Token<S>], S> {
    return new Parser(state => {
      const val = state.peek();
      return val !== END_OF_INPUT && pred(val)
        ? (state.advance(), consumedOK([val]))
        : emptyError(expected ?? '', val !== END_OF_INPUT ? `'${val}'` : 'end of input', state);
    });
  }

  static while<S>(pred: (c: Token<S>) => boolean, options?: {msg?: string, min?: number}): Parser<[Token<S>[]], S> {
    return new Parser(state => {
      const min = options?.min ?? 0;
      const msg = options?.msg ?? '';
      let acc: Token<S>[] = [];
      let peekedVal;
      while (peekedVal = state.peek(), peekedVal !== END_OF_INPUT && pred(peekedVal)) {
        // This cast works because we're only in this block if Tok is not END_OF_INPUT.
        acc.push(peekedVal as Token<S>);
        state.advance();
      }
      const consumed = acc.length > 0;
      const result: Result<[Token<S>[]], ParseError> = acc.length >= min
        ? Result.ok([acc])
        : Result.err(new ParseError(msg, 'insufficient characters', state.position));
      return {consumed, result};
    });
  }

  static joinWhile(pred: (c: string) => boolean, options?: {msg?: string, min?: number}): Parser<[string], Streamable<string>> {
    return Parser.while(pred, options).mapTop(val => val.join(''));
  }

  // TODO: Many of these methods create an extra temporary object from the
  // result of parse, which is not used outside of the function. Is there any
  // performance benefit to modifying this object in place an returning it?
  //
  // For example, map could be rewritten as:
  //
  //   map<T2 extends unknown[]>(f: (stack: T) => T2): Parser<T2> {
  //     return new Parser(state => {
  //       const res: ParseResult<any> = this.parse(state);
  //       res.result = res.result.map(f);
  //       return res as ParseResult<T2>;
  //     });
  //   }
  //
  // Note that this requires using some type assertions that are safe because
  // res is never referenced outside of the function, so the fact that is
  // initially a ParseResult<T> and then turned into a ParseResult<T2> does not
  // cause any problems.
  //
  // Further note: An initial benchmark suggested this doesn't do much, if
  // anything, to increase performance. And it seems to be significantly worse
  // in certain circumstances!

  map<T2 extends unknown[]>(f: (stack: T) => T2): Parser<T2,S> {
    return new Parser(state => {
      const res = this.parse(state);
      res.result.mapInPlace(f);
      return res as unknown as ParseResult<T2>;
    });
  }

  mapTop<V>(f: (top: Top<T>) => V): Parser<[V, ...Rest<T>],S> {
    return new Parser(state => {
      const res = this.parse(state);
      res.result.mapInPlace(stack => {
        stack = stack.slice() as any;
        stack[0] = f(stack[0] as Top<T>);
        return stack as unknown as [V, ...Rest<T>];
      });
      return res as any;
    });
  }

  mapError(f: (err: ParseError) => ParseError): Parser<T,S> {
    return new Parser(state => {
      const res = this.parse(state);
      res.result.mapErrorInPlace(f);
      return res;
    });
  }

  bind<T2 extends unknown[]>(f: (stack: T) => Parser<T2,S>): Parser<T2,S> {
    return new Parser(state => {
      const res = this.parse(state);
      if (res.result.isOK)
        return f(res.result.unwrap()).parse(state);
      else
        return res as unknown as ParseResult<T2>;
    });
  }

  then<T2 extends unknown[]>(parser: Parser<T2,S>): Parser<[...T, ...T2],S> {
    return new Parser(state => {
      const res = this.parse(state);
      if (res.result.isOK) {
        const vals1 = res.result.unwrap();
        const res2 = parser.parse(state);
        res2.result.mapInPlace(vals2 => [...vals1, ...vals2]);
        return res2 as unknown as ParseResult<[...T, ...T2]>;
      }
      else return res as unknown as ParseResult<[...T, ...T2]>;
    });
  }

  skip(parser: Parser<unknown[],S>): Parser<T,S> {
    return new Parser(state => {
      const res = this.parse(state);
      if (res.result.isOK)
        void parser.parse(state);
      return res;
    });
  }

  merge(): Parser<[[...T]],S> {
    return this.map(x => [x]);
  }

  expect(expected: string): Parser<T,S> {
    return new Parser(state => {
      const res = this.parse(state);
      res.result.mapErrorInPlace(err => err.copy({expected}));
      return res;
    });
  }

  stream<T2 extends unknown[]>(parser: Parser<T2,ToStream<T>>): Parser<T2,S> {
    throw new Error('stream not implemented yet')
    // return new Parser(state => {
    //   const {consumed, result} = this.parse(state);
    //   if (result.isOK) {
    //     const [stream] = result.unwrap() as unknown as [ToStream<T>];
    //     const state2 = new ParseState(stream) as ParseState<ToStream<T>>;
    //     const {result: result2} = parser.parse(state2);
    //     return {consumed, result: result2};
    //   }
    //   return {consumed, result} as unknown as ParseResult<T2>;
    // });
  }

  run(input: Stream<S>): Result<Top<T>, ParseError> {
    if (typeof input !== 'string')
      throw new Error('non-string parsers not implemented yet');
    const state = new ParseStateString(input) as unknown as ParseState<S>;
    return this.parse(state).result.map(stack => stack[0] as Top<T>);
  }

}

export const {any, end} = Parser;

// type StreamParser<T2 extends unknown[], T extends unknown[]> = T extends [(infer U)[]] ? Parser<T2, U[]> : never;

type ToStream<T extends unknown[]> = T extends [(infer U)[]] ? U[] : never;

// type StreamType<T extends unknown[]> = T extends [(infer U)[]] ? U : never;

/**
 * Takes a list of parsers, and tries them one at a time until either one
 * succeeds or one fails after consuming input.
 */
export function oneOf<T extends unknown[], S>(...parsers: Parser<T,S>[]): Parser<T,S> {
  return new Parser(state => {
    for (const parser of parsers) {
      const {consumed, result} = parser.parse(state);
      if (result.isOK || consumed)
        return {consumed, result};
    }
    return emptyError('', 'no matching parser', state);
  });
}

export function between<T extends unknown[], S>(start: Parser<unknown[],S>, inside: Parser<T,S>, end: Parser<unknown[],S>): Parser<T,S> {
  return skip(start).then(inside).skip(end);
}

export function skip<S>(parser: Parser<unknown[],S>): Parser<[],S> {
  return parser.map(_ => []);
}

export function char(c: string): Parser<[string], Streamable<string>> {
  return Parser.when(c2 => c === c2, `'${c}'`);
}

export const leftParen: Parser<[string], Streamable<string>> = char('(');
export const rightParen: Parser<[string], Streamable<string>> = char(')');

export function parens<T extends unknown[]>(parser: Parser<T, Streamable<string>>): Parser<T, Streamable<string>> {
  return between(leftParen, parser, rightParen);
}

export const leftBrace: Parser<[string], Streamable<string>> = char('{');
export const rightBrace: Parser<[string], Streamable<string>> = char('}');

export function braces<T extends unknown[]>(parser: Parser<T, Streamable<string>>): Parser<T, Streamable<string>> {
  return between(leftBrace, parser, rightBrace);
}

export const leftBracket: Parser<[string], Streamable<string>> = char('[');
export const rightBracket: Parser<[string], Streamable<string>> = char(']');

export function brackets<T extends unknown[]>(parser: Parser<T, Streamable<string>>): Parser<T, Streamable<string>> {
  return between(leftBracket, parser, rightBracket);
}

export const leftAngle: Parser<[string], Streamable<string>> = char('<');
export const rightAngle: Parser<[string], Streamable<string>> = char('>');

export function angles<T extends unknown[]>(parser: Parser<T, Streamable<string>>): Parser<T, Streamable<string>> {
  return between(leftAngle, parser, rightAngle);
}

export const doubleQuote: Parser<[string], Streamable<string>> = char('"');

export function doubleQuotes<T extends unknown[]>(parser: Parser<T, Streamable<string>>): Parser<T, Streamable<string>> {
  return between(doubleQuote, parser, doubleQuote);
}

export function exact(str: string): Parser<[string], Streamable<string>> {
  return new Parser(state =>{
    if (str === '') return emptyOK(['']);
    const first = state.peek();
    if (first !== str[0])
      return emptyError(`"${str}"`, first !== END_OF_INPUT ? `'${first}'` : 'end of input', state);
    let word = state.readMany(str.length);
    if (Array.isArray(word))
      word = word.join();
    if (word === str)
      return consumedOK([str]);
    else {
      const sharedLength = sharedPrefix(str, word).length;
      // Rewind the position to the point of divergence.
      state.position -= word.length - sharedLength;
      const diverged = word.slice(0, sharedLength + 1);
      return consumedError(`"${str}"`, `"${diverged}"`, state);
    }
  });
}

export function digit(radix: number): Parser<[number], Streamable<string>> {
  return Parser.when(isDigit).mapTop(c => parseInt(c, radix));
}

export const decimalDigit: Parser<[number], Streamable<string>> = digit(10);
export const hexDigit: Parser<[number], Streamable<string>> = digit(16);
export const octalDigit: Parser<[number], Streamable<string>> = digit(8);
export const binaryDigit: Parser<[number], Streamable<string>> = digit(2);

export function intBase(radix: number): Parser<[number], Streamable<string>> {
  return Parser.joinWhile(c => isDigit(c, radix), {min: 1}).mapTop(s => parseInt(s, radix));
}

export const decimalInt: Parser<[number], Streamable<string>> = intBase(10);
export const hexInt: Parser<[number], Streamable<string>> = intBase(16);
export const octalInt: Parser<[number], Streamable<string>> = intBase(8);
export const binaryInt: Parser<[number], Streamable<string>> = intBase(2);

export const int: Parser<[number], Streamable<string>> = decimalInt;

export const alpha: Parser<[string], Streamable<string>> = Parser.when(isAlpha);
export const lower: Parser<[string], Streamable<string>> = Parser.when(isLower);
export const upper: Parser<[string], Streamable<string>> = Parser.when(isUpper);
export const alphaNum: Parser<[string], Streamable<string>> = Parser.when(isAlphaNum);

export function separated<T,S>(item: Parser<[T],S>, sep: Parser<any,S>): Parser<[T[]],S> {
  return new Parser(state => {
    const acc: T[] = [];
    let {consumed, result} = item.parse(state);
    const parser: Parser<[T],S> = skip(sep).then(item);
    // Continue parsing until it fails.
    while (result.isOK) {
      acc.push(result.unwrap()[0]);
      ({consumed, result} = parser.parse(state));
    }
    // If the failure happened without consuming input, then we are fine and
    // return the accumulated result. Otherwise, return the error.
    if (consumed)
      // See the note on Parser.bind for why this is safe.
      return {consumed, result} as unknown as ParseResult<[T[]]>;
    else
      return {consumed, result: Result.ok([acc])}
  });
}

export function commaSep<T>(item: Parser<[T], Streamable<string>>): Parser<[T[]], Streamable<string>> {
  return separated(item, char(','));
}

export function semiSep<T>(item: Parser<[T], Streamable<string>>): Parser<[T[]], Streamable<string>> {
  return separated(item, char(','));
}

export function separatedPair<T1, T2, U extends unknown[]>(left: Parser<[T1], Streamable<string>>, sep: Parser<U, Streamable<string>>, right: Parser<[T2], Streamable<string>>): Parser<[[T1,T2]], Streamable<string>> {
  return left.skip(sep).then(right).merge();
}

export function many<T,S>(item: Parser<[T],S>): Parser<[T[]],S> {
  return new Parser(state => {
    const acc: T[] = [];
    let {consumed, result} = item.parse(state);
    while (result.isOK) {
      acc.push(result.unwrap()[0]);
      ({consumed, result} = item.parse(state));
    }
    if (consumed)
      return {consumed, result} as unknown as ParseResult<[T[]]>;
    else
      return {consumed, result: Result.ok([acc])};
  })
}

export function defer<T extends unknown[],S>(getParser: () => Parser<T,S>): Parser<T,S> {
  return new Parser(state => getParser().parse(state));
}

export const whitespace: Parser<[], string> = skip(Parser.while(isSpace));

const example = braces(char('x')).run("[x]");