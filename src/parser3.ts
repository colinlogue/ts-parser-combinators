import { isAlpha, isAlphaNum, isDigit, isLower, isSpace, isUpper } from "./char";
import { Result } from "./result";

export interface ParserConfig {
  skip?: Parser<[]>,
}

export interface ParseState {
  position: number,
}

export const initialState: ParseState = {
  position: 0,
}

export interface ParserOptions {
  skip?: Parser<[]>,
}

export class ParseError extends Error {

  msg: string;
  position: number;

  // This is valid according to MDN, but not according to the type files for Error.
  // constructor(msg: string, position: number, options?: {cause: Error}) {
  //   const message = `Parse error at position ${position}: ${msg}`;
  //   super(message, options);
  //   this.msg = msg;
  //   this.position = position;
  // }

  constructor(msg: string, position: number) {
    const message = `Parse error at position ${position}: ${msg}`;
    super(message);
    this.msg = msg;
    this.position = position;
  }

}

export interface ParseResult<S extends unknown[]> {
  stack: S,
  state: ParseState,
}

export type ParseFunc<S extends unknown[]> = (input: string, config: ParserConfig, state: ParseState) => ParseResult<S>;

export type Top<S extends unknown[]> = S extends [infer T, ...infer U] ? T : void;

export class Parser<S extends unknown[]> {

  _parse: ParseFunc<S>;

  constructor(parse: ParseFunc<S>) {
    this._parse = parse;
    // Object.freeze(this);
  }

  static empty: Parser<[]> = new Parser ((str, config, state) => ({stack: [], state}));

  static pure<S extends unknown[]>(stack: S): Parser<S> {
    return new Parser((str, config, state) => ({stack, state}));
  }

  static succeed<T>(val: T): Parser<[T]> {
    return new Parser((str, config, state) => ({stack: [val], state}));
  }

  static fail<S extends unknown[]>(msg: string): Parser<S> {
    return new Parser((str, config, state) => { throw new ParseError(msg, state.position); });
  }

  static chomp(n = 1): Parser<[string]> {
    return new Parser((str, config, state) => {
      const newPosition = n + state.position;
      if (str.length >= newPosition)
        return {
          stack: [str.slice(state.position, newPosition)],
          state: {position: newPosition},
        };
      else
        throw new ParseError('chomped past end of input', state.position);
    });
  }

  static chompMap<T>(f: (c: string) => T | null): Parser<[T]> {
    return Parser.chomp().bind(
      ([c]) => {
        const val = f(c);
        return val === null
          ? Parser.fail('chomp map failed')
          : Parser.succeed(val);
      }
    );
  }

  static chompIf(pred: (s: string) => boolean, n = 1): Parser<[string]> {
    return new Parser((str, config, state) => {
      const newPosition = n + state.position;
      if (str.length >= newPosition) {
        const word = str.slice(state.position, newPosition);
        if (pred(word))
          return {
            stack: [word],
            state: {position: newPosition},
          };
        else
          throw new ParseError('chomp predicate failed', state.position);
      }
      else
        throw new ParseError('chomped past end of input', state.position);
    });
  }

  static chompWhile(pred: (c: string) => boolean): Parser<[string]> {
    return new Parser((str, config, state) => {
      let i = state.position;
      while (i < str.length && pred(str[i])) ++i;
      return {
        stack: [str.slice(state.position, i)],
        state: {position: i},
      };
    });
  }

  parse(str: string, config: ParserConfig, state: ParseState): ParseResult<S> {
    if (config.skip)
      state = config.skip._parse(str, config, state).state;
    return this._parse(str, config, state);
  }

  map<S2 extends unknown[]>(f: (stack: S) => S2): Parser<S2> {
    return new Parser((str, config, state) => {
      const {stack, state: {position}} = this.parse(str, config, state);
      return {stack: f(stack), state: {position}};
    });
  }

  bind<S2 extends unknown[]>(f: (stack: S) => Parser<S2>): Parser<S2> {
    return new Parser((str, config, state) => {
      const {stack, state: {position}} = this.parse(str, config, state);
      const parser = f(stack);
      return parser.parse(str, config, {position});
    });
  }

  then<S2 extends unknown[]>(parser: Parser<S2>): Parser<[...S2, ...S]> {
    return new Parser((str, config, state) => {
      const {stack: stack1, state: state1} = this.parse(str, config, state);
      const {stack: stack2, state: state2} = parser.parse(str, config, state1);
      return {
        stack: [...stack2, ...stack1],
        state: state2,
      };
    });
  }

  ignore<S2 extends unknown[]>(parser: Parser<S2>): Parser<S> {
    return new Parser((str, config, state) => {
      const {stack: stack1, state: state1} = this.parse(str, config, state);
      const {state: state2} = parser.parse(str, config, state1);
      return {
        stack: stack1,
        state: state2,
      };
    });
  }

  assert(pred: (stack: S) => boolean, msg?: string): Parser<S> {
    return new Parser((str, config, state) => {
      const {stack, state: newState} = this.parse(str, config, state);
      if (pred(stack))
        return {stack, state: newState};
      else
        throw new ParseError(msg ?? 'assertion failed', state.position);
    });
  }

  or(alt: Parser<S>): Parser<S> {
    return new Parser((str, config, state) => {
      try {
        return this.parse(str, config, state);
      } catch (e) {
        if (e instanceof ParseError)
          return alt.parse(str, config, state);
        else
          throw e;
      }
    });
  }

  run(input: string, options?: ParserOptions): Result<Top<S>, ParseError> {
    const config: ParserConfig = {
      skip: options?.skip,
    };
    try {
      const {stack} = this.parse(input, config, initialState);
      return Result.ok(stack[0] as Top<S>);
    } catch (e) {
      if (e instanceof ParseError)
        return Result.err(e);
      else
        throw e;
    }
  }

  peek(input: string, config: ParserConfig, state: ParseState, n = 1): string {
    if (config.skip)
      state = config.skip._parse(input, config, state).state;
    return input.slice(state.position, state.position + n);
  }

  lookAhead(n = 1): Parser<[string]> {
    return new Parser((str, config, state) => ({stack: [this.peek(str, config, state, n)], state}));
  }

}

export const whitespace: Parser<[]> = Parser.pure([] as []).ignore(Parser.chompWhile(isSpace));

export function many<T>(item: Parser<[T]>, constraints?: {min?: number, max?: number}): Parser<[T[]]> {
  return new Parser((str, config, state) => {
    let min = constraints?.min ?? 0;
    let max = constraints?.max ?? Infinity;
    const acc: T[] = [];
    let keepGoing = true;
    while (acc.length < max) {
      // TODO: Use lookahead rather than backtracking.
      const {stack, state: newState} = optional(item).parse(str, config, state);
      state = newState;
      if (stack.length === 0)
        break;
      else
        acc.push(stack[0]);
    }
    return {stack: [acc], state};
  });
}

export function optional<S extends unknown[]>(parser: Parser<S>): Parser<S|[]> {
  return new Parser((str, config, state) => {
    try {
      return parser.parse(str, config, state);
    } catch (e) {
      if (e instanceof ParseError)
        return {stack: [] as S|[], state};
      else
        throw e;
    }
  })
}

export function delimited<U1 extends unknown[], S extends unknown[], U2 extends unknown[]>
  (start: Parser<U1>, inner: Parser<S>, end: Parser<U2>): Parser<S>
{
  return ignored(start)
    .then(inner)
    .ignore(end)
}

export function char(c: string): Parser<[string]> {
  return Parser.chompIf(c2 => c === c2);
}

export function exact(val: string): Parser<[string]> {
  return new Parser((str, config, state) => {
    if (str.startsWith(val))
      return {stack: [val], state: {position: state.position + val.length}};
    else
      throw new ParseError(`input does not start with ${val}`, state.position);
  });
}

export function digit(radix: number): Parser<[number]> {
  return Parser.chompMap(c => {
    let val = parseInt(c, radix);
    return isNaN(val)
      ? null
      : val;
    });
}

export const decimalDigit: Parser<[number]> = digit(10);
export const hexDigit: Parser<[number]> = digit(16);
export const octalDigit: Parser<[number]> = digit(8);
export const binaryDigit: Parser<[number]> = digit(2);

export function intBase(radix: number): Parser<[number]> {
  return Parser.chompWhile(c => isDigit(c, radix)).assert(([s]) => s !== '').map(([s]) => [parseInt(s, radix)]);
}

export const decimalInt: Parser<[number]> = intBase(10);
export const hexInt: Parser<[number]> = intBase(16);
export const octalInt: Parser<[number]> = intBase(8);
export const binaryInt: Parser<[number]> = intBase(2);

export const int: Parser<[number]> = decimalInt;

export const alpha: Parser<[string]> = Parser.chompIf(isAlpha);
export const lower: Parser<[string]> = Parser.chompIf(isLower);
export const upper: Parser<[string]> = Parser.chompIf(isUpper);
export const alphaNum: Parser<[string]> = Parser.chompIf(isAlphaNum);

export function parens<T extends unknown[]>(inside: Parser<T>): Parser<T> {
  return delimited(char('('), inside, char(')'));
}

export function braces<T extends unknown[]>(inside: Parser<T>): Parser<T> {
  return delimited(char('{'), inside, char('}'));
}

export function brackets<T extends unknown[]>(inside: Parser<T>): Parser<T> {
  return delimited(char('['), inside, char(']'));
}

export function angles<T extends unknown[]>(inside: Parser<T>): Parser<T> {
  return delimited(char('<'), inside, char('>'));
}

export function separated<T, U extends unknown[]>(item: Parser<[T]>, sep: Parser<U>): Parser<[T[]]> {
  return new Parser((str, config, state) => {
    const {stack, state: state1} = optional(item).parse(str, config, state);
    if (stack.length === 0)
      return {stack: [[]], state};
    const [val] = stack;
    const {stack: [vals], state: state2} = many(ignored(sep).then(item)).parse(str, config, state1);
    vals.unshift(val);
    return {stack: [vals], state: state2};
  });
}

export function commaSep<T>(item: Parser<[T]>): Parser<[T[]]> {
  return separated(item, char(','));
}

export function semiSep<T>(item: Parser<[T]>): Parser<[T[]]> {
  return separated(item, char(','));
}

export function separatedPair<T1, T2, U extends unknown[]>(left: Parser<[T1]>, sep: Parser<U>, right: Parser<[T2]>): Parser<[[T1,T2]]> {
  return left.ignore(sep).then(right).map(([r,l]) => [[l,r]]);
}

export function oneOf<T extends unknown[]>(...alts: Parser<T>[]): Parser<T> {
  return alts.reduce((prev, cur) => prev.or(cur));
}

export function ignored<U extends unknown[]>(parser: Parser<U>): Parser<[]> {
  return parser.map(_ => []);
}