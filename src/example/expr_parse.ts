import { char, defer, lower, many, oneOf, parens, Parser, skip, whitespace } from "../parser7";

type Var = string;

type Op = '+' | '-' | '*' | '/' | '^';

function isOp(c: string): c is Op {
  return c === '+' || c === '-' || c === '*' || c === '/' || c === '^';
}

interface BinaryOp {
  left: Expr,
  op: Op,
  right: Expr,
}

export type Expr = Var | BinaryOp

const LEFT = Symbol('left');
const RIGHT = Symbol('right');

type Assoc = typeof LEFT | typeof RIGHT;

interface AssociativityTable {
  [op1: string]: { [op2: string]: Assoc | void }
}

const assoc: AssociativityTable = {
  '^': {'^': RIGHT},
  '*': {'*': LEFT, '/': LEFT},
  '/': {'*': LEFT, '/': LEFT},
  '+': {'+': LEFT, '-': LEFT},
  '-': {'+': LEFT, '-': LEFT},
};

type BindStrengthTable = {
  [op in Op]: number;
};

const bindStrength: BindStrengthTable = {
  '^': 3,
  '*': 2,
  '/': 2,
  '+': 1,
  '-': 1,
};

export const variable: Parser<[Var]> = lower;

export const operator: Parser<[Op]> = Parser.when<string>(isOp) as Parser<[Op]>;

export const atom: Parser<[Expr]> =
  oneOf(
    variable,
    parens(defer(() => expr))
  )
  .skip(whitespace);

const opThenAtom: Parser<[[Op, Expr]]> =
  operator
    .skip(whitespace)
    .then(atom)
    .merge();

export const expr: Parser<[Expr]> =
  skip(whitespace)
    .then(atom)
    .then(many(opThenAtom))
    .map(args => [collect(...args)]);

function collect(left: Expr, ops: [Op, Expr][]): Expr {

  // The general idea here is that we have a list of expressions interspersed
  // with operators, and each expression binds more tightly to the operator on
  // its right or left. We process the list from left to right, and on each
  // step we have:
  //
  //   -  An expression `e`
  //   -  The next operator and expression pair `(op_R, e_R)`
  //   -  A stack of previous expression and operator pairs `(e_L, op_L)`
  //
  // We compare the binding strength of the operator on the left (the one at
  // the top of the stack, `op_L`) to the one on the right (`op_R`). If they
  // have the same precedence, check the pair in the associativity table. If
  // they don't have an associativity, then it's an invalid use of operators
  // and is an error.
  //
  // If the left binds tighter, pop `(e_L, op_L)` from the stack, set `e` to
  // the binary expression `e_L op_L e`, and process again before advancing to
  // the next elements in the list.
  //
  // If the right binds tighter, push `(e, op_R)` to the stack, and set `e` to
  // be `e_R` for the next iteration. Continue with the next pair in the list.
  //
  // Note the symmetry: If we invert the precedence operation and reverse the
  // list, we get a mirror of the tree (the same structure, but with the left
  // and right branches flipped).
  //
  // ====== Example: a * b + c ^ d ^ e * f =====================================
  // a * b                          Starting from the left
  // [a*] b + c                     Nothing to the left of a, so it binds to *
  // (a*b) + c                      * binds more tightly than +, so reduce
  // [(a*b)+] c ^ d                 Nothing to the left again, so bind +
  // [(a*b)+] [c^] d ^ e            ^ has highest precedence, so c binds to it
  // [(a*b)+] [c^] [d^] e * f       ^ associates to right, so d binds to next ^
  // [(a*b)+] [c^] (d^e) * f        ^ binds tighter than *, so reduce
  // [(a*b)+] (c^(d^e)) * f         Continue reducing
  // (a*b) + ((c^(d^e)) * f)

  const stack: [Expr, Op][] = [];

  const getDir: (op2: Op) => Assoc|void = op2 => {
    const idx = stack.length - 1;
    if (idx < 0)
      return RIGHT;
    const [_, op1] = stack[idx];
    const str1 = bindStrength[op1];
    const str2 = bindStrength[op2];
    if (str1 > str2)
      return LEFT;
    else if (str2 > str1)
      return RIGHT;
    else
      return assoc[op1][op2];
  }

  const process = (op: Op, right: Expr) => {
    const dir = getDir(op);

    if (dir === LEFT) {
      const [e1, op1] = stack.pop() as [Expr, Op];
      left = {left: e1, op: op1, right: left};
      process(op, right);
    }
    else if (dir === RIGHT) {
      stack.push([left, op]);
      left = right;
    }
    else
      throw new Error("operator error");
  }

  for (const [op, right] of ops)
    process(op, right);

  let right = left;
  let op: Op;
  while (stack.length > 0) {
    [left, op] = stack.pop() as [Expr, Op];
    right = {left, op, right};
  }

  return right;

}

export function equalExpr(e1: Expr, e2: Expr): boolean {

  if (typeof e1 === 'string') {

    if (typeof e2 === 'string')
      return e1 === e2;

    return false;

  }

  if (typeof e2 === 'string')
    return false;

  return e1.op === e2.op
    && equalExpr(e1.left, e2.left)
    && equalExpr(e1.right, e2.right);

}