
export interface result_ok<T> {
  ok: true,
  value: T,
}

export interface result_err<E> {
  ok: false,
  error: E,
}

export type result<T,E> =
  | result_ok<T>
  | result_err<E>

export class Result<T,E> {

  private _res: result<T,E>;

  private constructor(res: result<T,E>) {
    this._res = res;
  }

  static ok<T,E>(value: T): Result<T,E> {
    return new Result({ok: true, value});
  }

  static err<T,E>(error: E): Result<T,E> {
    return new Result({ok: false, error});
  }

  static join<T,E>(res: Result<Result<T,E>, E>): Result<T,E> {
    return res.isOK
      ? res.unwrap()
      : Result.err(res.unwrapError())
  }

  unwrap() {
    if (this._res.ok)
      return this._res.value;
    else
      throw new Error("failed to unwrap Result");
  }

  unwrapError() {
    if (!this._res.ok)
      return this._res.error;
    else
      throw new Error("failed to unwrap error from Result");
  }

  map<T2>(f: (param: T) => T2 ): Result<T2,E> {
    if (this._res.ok)
      return Result.ok(f(this._res.value));
    else
      return Result.err(this._res.error);
  }

  mapInPlace<T2>(f: (param: T) => T2 ): Result<T2,E> {
    const self = this as unknown as Result<T2, E>;
    if (this._res.ok)
      (self._res as result_ok<T2>).value = f(this._res.value);
    return self;
  }

  mapError<E2>(f: (err: E) => E2): Result<T,E2> {
    if (!this._res.ok)
      return Result.err(f(this._res.error));
    else
      return Result.ok(this._res.value);
  }

  mapErrorInPlace<E2>(f: (err: E) => E2) {
    const self = this as unknown as Result<T,E2>;
    if (!this._res.ok)
      (self._res as result_err<E2>).error = f(this._res.error);
    return self;
  }

  bind<T2>(f: (param: T) => Result<T2,E>) {
    if (this._res.ok)
      return f(this._res.value);
    else
      return Result.err(this._res.error);
  }

  bindInPlace<T2>(f: (val: T) => Result<T2,E>) {
    const self = this as unknown as Result<T2,E>;
    if (this._res.ok)
      self._res = f(this._res.value)._res;
    return self;
  }

  withDefault(val: T): T {
    if (this._res.ok)
      return this._res.value;
    else
      return val;
  }

  bimap<T2,E2>(f1: (param: T) => T2, f2: (param: E) => E2): Result<T2,E2> {
    if (this._res.ok)
      return Result.ok(f1(this._res.value));
    else
      return Result.err(f2(this._res.error));
  }

  match<U>(f1: (val: T) => U, f2: (err: E) => U): U {
    if (this._res.ok)
      return f1(this._res.value);
    else
      return f2(this._res.error);
  }

  get isOK(): boolean {
    return this._res.ok;
  }

  get isErr(): boolean {
    return !this._res.ok;
  }

}
