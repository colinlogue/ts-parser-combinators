const benchmark = require('nodemark');
const parser7 = require('../build/parser7');
const parser7_opt = require('../build/parser7_opt');
const {writeFile} = require('fs/promises');

const test_vals = [];
for (let i = 0; i < 1; ++i) {
  const size = Math.floor(Math.random() * 1000);
  const val = [];
  for (let j = 0; j < size; ++j) {
    val.push(j);
  }
  test_vals.push({expected: val, input: '[' + val.join(',') + ']'});
}

function getTestVal() {
  const size = Math.floor(Math.random() * 1000);
  const val = [];
  for (let j = 0; j < size; ++j) {
    val.push(Math.floor(Math.random() * 10000000));
  }
  return {expected: val, input: '[' + val.join(',') + ']'}
}

function checkResult(actual, expected, input) {
  let error = null;
  if (!actual) error = 'actual was null';
  else if (actual.length !== expected.length) error = 'result length mismatch';
  else for (let i = 0; i < actual.length; ++i) {
    if (actual[i] !== expected[i]) {
      error = 'mismatched elements';
      break;
    }
  }
  if (error) {
    console.log(' === input ===\n', input, '\n\n\n === actual ===\n', actual, '\n\n\n === expected ===\n', expected);
    writeFile('fail_actual.txt', actual);
    writeFile('fail_expected.txt', expected);
    throw new Error(error);
  }
}

let testVal;

function setup() {
  testVal = getTestVal();
}

// Test 1: List of numbers

function regexParse(input) {
  if (/^\[(?:\d+(?:,\d+)*)*\]$/.test(input)) {
    const result = input.match(/\d+/g);
    if (result === null)
      return [];
    else
      return result.map(x => parseInt(x, 10));
  }
  return null;
}

function regexVersion() {

  const {input, expected} = testVal;
  const actual = regexParse(input);
  checkResult(actual, expected, input);

}

const p1 = parser7.brackets(parser7.commaSep(parser7.decimalInt));

function parserVersion() {

  const {input, expected} = testVal;
  const actual = p1.run(input).unwrap();
  checkResult(actual, expected, input);

}

const p1_opt = parser7_opt.brackets(parser7_opt.commaSep(parser7_opt.decimalInt));

function parserVersion_opt() {

  const {input, expected} = testVal;
  const actual = p1_opt.run(input).unwrap();
  checkResult(actual, expected, input);

}

console.log('running regex benchmarks...');
const regexResults = benchmark(regexVersion, setup);
console.log('done\n');

console.log('running parser benchmarks...')
const parserResults = benchmark(parserVersion, setup);
console.log('done\n');

console.log('running optimized parser benchmarks...')
const parserResults_opt = benchmark(parserVersion_opt, setup);
console.log('done\n');

console.log('regex  : ', regexResults);
console.log('parser : ', parserResults);
console.log('opt    : ', parserResults_opt);